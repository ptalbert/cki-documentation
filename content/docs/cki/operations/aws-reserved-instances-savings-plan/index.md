---
title: "Renewing AWS savings plans and instance reservations"
description: >
  How to safely navigate AWS for the scary task of committing for future spent
companion: true
---

## Problem

You receive alerts for CkiSavingsPlans or CkiReservedInstances.

## Steps

1. Take a look at the Grafana dashboard to determine the savings plan/instance
   reservation that is going to expire. Confirm this in the AWS web UI for
   [savings plans][aws-sp] and [reserved instances][aws-ri].

1. If this is a shared AWS account, obtain agreement of all stakeholders about
   the way forward. Options include

   - renew the savings plan or instance reservation
   - convert the instance reservation into a savings plan
   - move the covered workloads off the AWS account
   - let the savings plan or instance reservation expire, e.g. because covered
     workloads will change within the next year

1. For renewing a savings plan, select the appropriate savings plan in the
   list. Click on "Actions" -> "Renew savings plan". Sanity-check the
   displayed amount, and then click on "Submit order".

1. For renewing an instance reservation, select the appropriate instance
   reservation in the list. Record the instance count associated with the
   reservation. Click on "Actions" -> "Renew reserved instances" and correct
   the quantity to the previously recorded instance count. Sanity-check the
   displayed average monthly recurring costs, and then click on "Order all".

1. **Both savings plans and instance reservations can be deleted before they
   come into effect, i.e. as long as they are in state `queued`.**

[aws-sp]: https://console.aws.amazon.com/cost-management/home#/savings-plans/inventory
[aws-ri]: https://console.aws.amazon.com/ec2/home#ReservedInstances:

{{% include "internal.md" %}}
