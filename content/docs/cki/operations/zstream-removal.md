---
title: Removing a kernel zstream
description: Steps to do when support for a kernel stream has to be removed.
---

## Background

Every once in a while, a RHEL stream reaches its end of life (EOL). In that
case, CKI needs to update its setup to match this.

## Steps to take

1. Remove the zstream branches from the [gitlab-ci-bot config].
2. Remove the zstream tree from [kpet-db]. Also update the [nvr tree mapping
   configuration] accordingly.
3. Remove the [builder container image] for it.
4. Remove the container image from the `.pipeline_images` job template in the
   CKI GitLab CI/CD templates in [cki-lib].
5. Remove the container images from the [gitlab.com] and [quay.io] container
   registries. Check the `Usage Logs` tab to make sure the container images are
   really not in use anymore.

[kpet-db]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/tree/main/trees
[nvr tree mapping configuration]: https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/blob/main/nvr-tree-mapping.yml
[builder container image]: pipeline-images.md
[gitlab-ci-bot config]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/openshift/gitlab-ci-bot/10-configmap.yml.j2.d/bot-config.yml
[cki-lib]: https://gitlab.com/cki-project/cki-lib/-/blob/main/.gitlab/ci_templates/cki-templates.yml
[gitlab.com]: https://gitlab.com/cki-project/containers/container_registry
[quay.io]: https://quay.io/organization/cki
